package fr.lr.service;

import fr.lr.connexion.BDDConnect;
import fr.lr.jobspersonne.*;
import fr.lr.util.DateTools;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author HUYNH ANH-Khoa
 */
@ManagedBean
@SessionScoped
public class PersonneBean implements Serializable {

    private ArrayList<Pays> paysList;
    private ArrayList<Ville> villeList;
    private ArrayList<TypeDeVoie> typeDeVoieList;
    private ArrayList<AdressePostale> adressePostaleList;
    private ArrayList<TypeDeContact> typeDeContactList;
    private ArrayList<Personne> personneList;
    private ArrayList<Email> emailList;
    private ArrayList<Telephone> telephoneList;
    private Personne selectedPersonne;
    private Date selectedDate;
    private Pays selectedPays;
    private Ville selectedVille;
    private TypeDeVoie selectedTypeDeVoie;

    public PersonneBean() {

        BDDConnect.createConnection("Admin", "Admin");
        this.paysList = DAOFactory.getPaysDAO().getAll();
        this.villeList = DAOFactory.getVilleDAO().getAll();
        this.typeDeVoieList = DAOFactory.getTypeDeVoieDAO().getAll();
        this.adressePostaleList = DAOFactory.getAdressePostaleDAO().getAll();
        this.typeDeContactList = DAOFactory.getTypeDeContactDAO().getAll();
        this.personneList = DAOFactory.getPersonneDAO().getAll();
        this.emailList = DAOFactory.getEmailDAO().getAll();
        this.telephoneList = DAOFactory.getTelephoneDAO().getAll();
    }

    public ArrayList<Pays> getPaysList() {
        return paysList;
    }

    public void setPaysList(ArrayList<Pays> paysList) {
        this.paysList = paysList;
    }

    public ArrayList<Ville> getVilleList() {
        return villeList;
    }

    public void setVilleList(ArrayList<Ville> villeList) {
        this.villeList = villeList;
    }

    public ArrayList<TypeDeVoie> getTypeDeVoieList() {
        return typeDeVoieList;
    }

    public void setTypeDeVoieList(ArrayList<TypeDeVoie> typeDeVoieList) {
        this.typeDeVoieList = typeDeVoieList;
    }

    public ArrayList<AdressePostale> getAdressePostaleList() {
        return adressePostaleList;
    }

    public void setAdressePostaleList(ArrayList<AdressePostale> adressePostaleList) {
        this.adressePostaleList = adressePostaleList;
    }

    public ArrayList<TypeDeContact> getTypeDeContactList() {
        return typeDeContactList;
    }

    public void setTypeDeContactList(ArrayList<TypeDeContact> typeDeContactList) {
        this.typeDeContactList = typeDeContactList;
    }

    public ArrayList<Personne> getPersonneList() {
        return personneList;
    }

    public void setPersonneList(ArrayList<Personne> personneList) {
        this.personneList = personneList;
    }

    public ArrayList<Email> getEmailList() {
        return emailList;
    }

    public void setEmailList(ArrayList<Email> emailList) {
        this.emailList = emailList;
    }

    public ArrayList<Telephone> getTelephoneList() {
        return telephoneList;
    }

    public void setTelephoneList(ArrayList<Telephone> telephoneList) {
        this.telephoneList = telephoneList;
    }

    public Personne getSelectedPersonne() {
        return selectedPersonne;
    }

    public void setSelectedPersonne(Personne selectedPersonne) {
        this.selectedPersonne = selectedPersonne;
    }

    public Date getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    }

    public Pays getSelectedPays() {
        return selectedPays;
    }

    public void setSelectedPays(Pays selectedPays) {
        this.selectedPays = selectedPays;
    }

    public Ville getSelectedVille() {
        return selectedVille;
    }

    public void setSelectedVille(Ville selectedVille) {
        this.selectedVille = selectedVille;
    }

    public TypeDeVoie getSelectedTypeDeVoie() {
        return selectedTypeDeVoie;
    }

    public void setSelectedTypeDeVoie(TypeDeVoie selectedTypeDeVoie) {
        this.selectedTypeDeVoie = selectedTypeDeVoie;
    }

    public void onSelect(Personne selectedPersonne) {//@author Mathias-Auguste "Marion" Verstaevel-Magnier
        setSelectedPersonne(selectedPersonne);
        setSelectedDate(DateTools.localeDateToDate(selectedPersonne.getDateDeNaissance()));
        setSelectedTypeDeVoie(selectedPersonne.getAdressePostale().getTypeDeVoie());
        setSelectedPays(selectedPersonne.getAdressePostale().getVille().getPays());
        setSelectedVille(selectedPersonne.getAdressePostale().getVille());
    }

    public void updatePersonne()
    {
        if (selectedPersonne.getIdPersonne() == 0)
        {
            DAOFactory.getPersonneDAO().insert(selectedPersonne);
        } else {
            DAOFactory.getPersonneDAO().update(selectedPersonne);
        }
    }

    public void onChangeVille() {

        if (selectedPays != null)
            villeList = DAOFactory.getVilleDAO().getByPays(selectedPays.getIdPays());
        else
            villeList = DAOFactory.getVilleDAO().getAll();

    }


}