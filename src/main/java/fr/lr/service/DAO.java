package fr.lr.service;

import java.sql.Connection;
import java.util.ArrayList;

/**
 * @author HUYNH ANH-Khoa
 */
public abstract class DAO<T> {

    protected Connection connexion;

    public DAO(Connection connexion) {

        this.connexion = connexion;
    }

    public abstract T getById(int id);

    public abstract ArrayList<T> getAll();

    public abstract boolean insert(T object);

    public abstract boolean update(T object);

    public abstract boolean delete(T object);
}