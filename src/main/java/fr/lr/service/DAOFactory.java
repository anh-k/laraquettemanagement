package fr.lr.service;

import fr.lr.connexion.BDDConnect;
import fr.lr.daopersonne.*;

import java.sql.Connection;

/**
 * @author HUYNH ANH-Khoa & Marion
 */
public class DAOFactory {

	private DAOFactory() {
        System.out.print("");}
	
    private static final Connection connexion = BDDConnect.getInstance();

    public static PaysDAO getPaysDAO () {
        return new PaysDAO(connexion);
    }

    public static VilleDAO getVilleDAO () {
        return new VilleDAO (connexion);
    }
    
    public static TypeDeVoieDAO getTypeDeVoieDAO () {
        return new TypeDeVoieDAO (connexion);
    }

    public static TypeDeContactDAO getTypeDeContactDAO () {
        return new TypeDeContactDAO (connexion);
    }
    
    public static AdressePostaleDAO getAdressePostaleDAO () {
        return new AdressePostaleDAO (connexion);
    }
    
    public static PersonneDAO getPersonneDAO () {
        return new PersonneDAO (connexion);
    }
    
    public static TelephoneDAO getTelephoneDAO () {
        return new TelephoneDAO (connexion);
    }

    public static EmailDAO getEmailDAO () { 
    	return new EmailDAO(connexion);
    }
}
