package fr.lr.connexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Marion
 */


public class BDDConnect {


	// Declarer l'objet connection
	private static Connection connexion;

	/**
	 * Sert � se connecter � la base de donn�es.<br/>
	 * Il faut y indiquer le mot de passe et l'identifiant du<br/>
	 * compte utilisateur sur la base de donn�es.<br/>
	 */
	private BDDConnect() {
		// Se connecter avec l'url, le mot de passe et le nom d'utilisateur donn�
		try {
			Class.forName("org.postgresql.Driver");
			String dbURL = "jdbc:postgresql://10.115.58.21:5432/LaRaquette";
			setConnexion(dbURL, "Admin", "Admin");
		}

		// Gestion des erreur
		catch (Exception e) {e.printStackTrace();
			System.out.println("bdd inateignable. faites un e.printStackTrace(); ");
		}
	}

	/**
	 *  R�cup�rer l'instance de connection.<br/>
	 *  Retourne une connection.<br/>
	 *  Elle est nulle si elle n'as pas �tait g�n�r�e.
	 */
	public static synchronized Connection getInstance() {
		return connexion;
	}

	/**
	 * A utiliser pour creer la connection. <br/>
	 * N�c�ssite l'indication du nom d'utilisateur en premier argument<br/>
	 * et du mot de passe en second argument<br/>
	 * Retourne la connection
	 */
	public static synchronized Connection createConnection(String user, String pass) {
		new BDDConnect();
		return connexion;
	}
	
	/**
	 * Fonction pour �teindre la connection<br/>
	 * A utiliser lors de la d�connection<br/>
	 * Retourne un booleen indiquant la r�ussite de l'op�ration
	 */
	public static synchronized boolean closeConnection() {
		boolean isOki = true;
		try {
			connexion.close();
		} catch (SQLException e) {
			isOki = false;
		}
		return isOki;
	}

	// G�n�rer la connection
	private static void setConnexion(String dbURL, String user, String pass) throws SQLException {
		connexion = DriverManager.getConnection(dbURL, user, pass);
	}
	
}
