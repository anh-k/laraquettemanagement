package fr.lr.daopersonne;

import fr.lr.jobspersonne.TypeDeContact;
import fr.lr.service.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author HUYNH ANH-Khoa & Marion
 */
public class TypeDeContactDAO extends DAO<TypeDeContact> {
    public TypeDeContactDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public TypeDeContact getById(int id) {
        ResultSet rs;
        ArrayList<TypeDeContact> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select * from typecontact where id_typecontact = "+id+" ";
            rs = stmt.executeQuery(strCmd);

            TypeDeContact typeDeContactLu = new TypeDeContact();
            while (rs.next()) {
                if (typeDeContactLu.getIdTypeDeContact() != rs.getInt(1)) {
                    typeDeContactLu = new TypeDeContact(rs.getInt(1), rs.getString(2));
                    liste.add(typeDeContactLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste.get(0);
    }

    @Override
    public ArrayList<TypeDeContact> getAll() {
        ResultSet rs;
        ArrayList<TypeDeContact> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select * from typecontact";
            rs = stmt.executeQuery(strCmd);

            TypeDeContact typeDeContactLu = new TypeDeContact();
            while (rs.next()) {
                if (typeDeContactLu.getIdTypeDeContact() != rs.getInt(1)) {
                    typeDeContactLu = new TypeDeContact(rs.getInt(1), rs.getString(2));
                    liste.add(typeDeContactLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public boolean insert(TypeDeContact object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "insert into TypeContact (libelle)"
            		+ " values ('" + object.getNomTypeDeContact() + "')";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean update(TypeDeContact object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column
            
            String strCmd = "update TypeContact "
            		+ "set libelle = " + object.getNomTypeDeContact() +" , "
            		+ " where id_TypeContact = "+ object.getIdTypeDeContact() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean delete(TypeDeContact object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "delete from TypeContact where id_TypeContact = "+ object.getIdTypeDeContact() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }
}
