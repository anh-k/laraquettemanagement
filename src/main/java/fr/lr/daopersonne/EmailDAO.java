package fr.lr.daopersonne;

import fr.lr.jobspersonne.Email;
import fr.lr.service.DAO;
import fr.lr.service.DAOFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author HUYNH ANH-Khoa & Marion
 */
public class EmailDAO extends DAO<Email> {
    public EmailDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Email getById(int id) {
        return null;
    }

    @Override
    public ArrayList<Email> getAll() {
        ResultSet rs;
        ArrayList<Email> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select id_mail,mail,mot_de_passe_crypte,mail.id_personnes,mail.id_typecontact from mail "
                            + "join typecontact ON typecontact.id_typecontact = mail.id_typecontact "
                            + "join personnes ON personnes.id_personnes = mail.id_personnes "
                            + "order by id_mail,mail,mot_de_passe_crypte,id_personnes,id_typecontact";
            rs = stmt.executeQuery(strCmd);

            Email emailLu = new Email();
            while (rs.next()) {
                if (emailLu.getIdEmail() != rs.getInt(1)) {
                    emailLu = new Email(rs.getInt(1),
                            rs.getString(2),
                            rs.getString(3),
                            DAOFactory.getTypeDeContactDAO().getById(rs.getInt(5)),
                            DAOFactory.getPersonneDAO().getById(rs.getInt(4)));
                    liste.add(emailLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public boolean insert(Email object) {
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column
            
            String strCmd = "insert into mail (mail, mot_de_passe_crypte, id_personnes, "
            		+ "id_typecontact) values ("
                    + "'" + object.getLibelleEmail() + "',"
                    + "'" + object.getMdpCrypte() + "',"
            		+ "'" + object.getPersonne().getIdPersonne() + "',"
            		+ "'" + object.getTypeDeContact().getIdTypeDeContact() + "')";
            stmt.execute(strCmd);
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean update(Email object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column
            
            String strCmd = "update mail "
            		+ "set mail = " + object.getLibelleEmail() +" , "
                    + "set mot_de_passe_crypte = " + object.getMdpCrypte() +" , "
                    + "set id_personnes = " + object.getPersonne().getIdPersonne() +" , "
            		+ " where id_typecontact = "+ object.getTypeDeContact().getIdTypeDeContact() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean delete(Email object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "delete from mail where id_mail = "+ object.getIdEmail() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    //---------------------------------------------------------------------------------------------
    public ArrayList<Email> getByPersonne(int idPersonne) {
        ResultSet rs;
        ArrayList<Email> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select id_mail,mail,mot_de_passe_crypte,mail.id_personnes,mail.id_typecontact from mail "
                            + "join typecontact ON typecontact.id_typecontact = mail.id_typecontact "
                            + "join personnes ON personnes.id_personnes = mail.id_personnes "
                            + "where mail.id_personnes = "+ idPersonne +" "
                            + "order by id_mail,mail,mot_de_passe_crypte,id_personnes,id_typecontact";
            rs = stmt.executeQuery(strCmd);

            Email emailLu = new Email();
            while (rs.next()) {
                if (emailLu.getIdEmail() != rs.getInt(1)) {
                    emailLu = new Email(rs.getInt(1),
                            rs.getString(2),
                            rs.getString(3),
                            DAOFactory.getTypeDeContactDAO().getById(rs.getInt(5)),
                            DAOFactory.getPersonneDAO().getById(rs.getInt(4)));
                    liste.add(emailLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }
}
