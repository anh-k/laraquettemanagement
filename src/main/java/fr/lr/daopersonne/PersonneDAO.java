package fr.lr.daopersonne;

import fr.lr.jobspersonne.Personne;
import fr.lr.service.DAO;
import fr.lr.service.DAOFactory;
import java.sql.*;
import java.util.ArrayList;

/**
 * @author HUYNH ANH-Khoa & Marion
 */
public class PersonneDAO extends DAO<Personne> {
    public PersonneDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Personne getById(int id) {
        ResultSet rs;
        ArrayList<Personne> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select id_personnes, nom, prenom, date_de_naissance, estfemme, profession, "
                            + " id_adresse_postale from personnes "
                            + " where id_personnes = " + id + " "
                            + " order by nom,prenom";
            rs = stmt.executeQuery(strCmd);

            Personne personneLu = new Personne();
            while (rs.next()) {
                if (personneLu.getIdPersonne() != rs.getInt(1)) {


                    personneLu = new Personne(rs.getInt(1), rs.getString(2), rs.getString(3),
                            rs.getDate(4).toLocalDate(), rs.getBoolean(5), rs.getString(6),
                            DAOFactory.getAdressePostaleDAO().getById(rs.getInt(7)));


                    liste.add(personneLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste.get(0);
    }

    @Override
    public ArrayList<Personne> getAll() {
        ResultSet rs;
        ArrayList<Personne> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select id_personnes, nom, prenom, date_de_naissance, estfemme, profession, id_adresse_postale from personnes "
                            + "order by nom,prenom";
            rs = stmt.executeQuery(strCmd);

            Personne personneLu = new Personne();
            while (rs.next()) {
                if (personneLu.getIdPersonne() != rs.getInt(1)) {


                    personneLu = new Personne(rs.getInt(1), rs.getString(2), rs.getString(3),
                            rs.getDate(4).toLocalDate(), rs.getBoolean(5), rs.getString(6),
                            DAOFactory.getAdressePostaleDAO().getById(rs.getInt(7)));


                    liste.add(personneLu);
                }

            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public boolean insert(Personne object) {
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "insert into personnes (nom, prenom, "
                    + "date_de_naissance, estfemme, profession, id_adresse_postale)"
                    + " values ('" + object.getNom() + "','" + object.getPrenom() + "',"
                    + "'" + object.getDateDeNaissance() + "','" + object.isFemmeBoolean() + "',"
                    + "'" + object.getProfession() + "','"
                    + object.getAdressePostale().getIdAdressePostale() + "')";
            stmt.execute(strCmd);

            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }
    
    public int insertGetId(Personne object) {
        ResultSet rs;
        int inserted = 0;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "insert into personnes (nom, prenom, "
                    + "date_de_naissance, estfemme, profession, id_adresse_postale)"
                    + " values ('" + object.getNom() + "','" + object.getPrenom() + "',"
                    + "'" + object.getDateDeNaissance() + "','" + object.isFemmeBoolean() + "',"
                    + "'" + object.getProfession() + "','"
                    + object.getAdressePostale().getIdAdressePostale() +"') RETURNING id_personnes ;";
            rs = stmt.executeQuery(strCmd);
            rs.next();
            inserted = rs.getInt(1);
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }
    
    @Override
    public boolean update(Personne object) {
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "update personnes "
                    + "set nom = '" + object.getNom() + "' , "
                    + " prenom = '" + object.getPrenom() + "' , "
                    + " date_de_naissance = '" + object.getDateDeNaissance() + "' , "
                    + " estfemme = " + object.isFemmeBoolean() + " , "
                    + " profession = '" + object.getProfession() + "' , "
                    + " id_adresse_postale = " + object.getAdressePostale().getIdAdressePostale() + "  "
                    + " where id_personnes = " + object.getIdPersonne() + " ";
            stmt.execute(strCmd);

            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean delete(Personne object) {
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "delete from tel where id_Personnes = " + object.getIdPersonne() + " ";
            stmt.execute(strCmd);

            strCmd = "delete from mail where id_Personnes = " + object.getIdPersonne() + " ";
            stmt.execute(strCmd);
            
            strCmd = "delete from Personnes where id_Personnes = " + object.getIdPersonne() + " ";
            stmt.execute(strCmd);

            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }


}
