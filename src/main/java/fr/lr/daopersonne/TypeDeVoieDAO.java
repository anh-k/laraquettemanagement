package fr.lr.daopersonne;

import fr.lr.jobspersonne.TypeDeVoie;
import fr.lr.service.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author HUYNH ANH-Khoa & Marion
 */
public class TypeDeVoieDAO extends DAO<TypeDeVoie> {
    public TypeDeVoieDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public TypeDeVoie getById(int id) {
        ResultSet rs;
        ArrayList<TypeDeVoie> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select * from type_de_voie where id_type_de_voie = "+ id +" ";
            rs = stmt.executeQuery(strCmd);

            TypeDeVoie typeDeVoieLu = new TypeDeVoie();
            while (rs.next()) {
                if (typeDeVoieLu.getIdTypeDeVoie() != rs.getInt(1)) {
                    typeDeVoieLu = new TypeDeVoie(rs.getInt(1), rs.getString(2));
                    liste.add(typeDeVoieLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste.get(0);
    }

    @Override
    public ArrayList<TypeDeVoie> getAll() {
        ResultSet rs;
        ArrayList<TypeDeVoie> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select * from type_de_voie";
            rs = stmt.executeQuery(strCmd);

            TypeDeVoie typeDeVoieLu = new TypeDeVoie();
            while (rs.next()) {
                if (typeDeVoieLu.getIdTypeDeVoie() != rs.getInt(1)) {
                    typeDeVoieLu = new TypeDeVoie(rs.getInt(1), rs.getString(2));
                    liste.add(typeDeVoieLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public boolean insert(TypeDeVoie object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "insert into TypeDeVoie (libelle)"
            		+ " values ('" + object.getNomTypeDeVoie() + "')";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean update(TypeDeVoie object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column
            
            String strCmd = "update TypeDeVoie "
            		+ "set libelle = " + object.getNomTypeDeVoie() +" , "
            		+ " where id_type_de_voie = "+ object.getIdTypeDeVoie() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean delete(TypeDeVoie object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "delete from TypeDeVoie where id_type_de_voie = "+ object.getIdTypeDeVoie() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }
}
