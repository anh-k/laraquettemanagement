package fr.lr.daopersonne;

import fr.lr.jobspersonne.Pays;
import fr.lr.service.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author HUYNH ANH-Khoa & Marion
 */
public class PaysDAO extends DAO<Pays> {
	public PaysDAO(Connection connexion) {
		super(connexion);
	}

	@Override
	public Pays getById(int id) {
		ResultSet rs;
		ArrayList<Pays> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();
            // Set column
            String strCmd =
                    "select pays.id_pays, nom_pays from pays "
                    +"where pays.id_pays = "+ id +" order by nom_pays";
            rs = stmt.executeQuery(strCmd);
            Pays paysLu = new Pays();
            while (rs.next()) {
                if (paysLu.getIdPays() != rs.getInt(1)) {
                    paysLu = new Pays(rs.getInt(1), rs.getString(2));


                    liste.add(paysLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste.get(0);
	}

	@Override
	public ArrayList<Pays> getAll() {
		ResultSet rs;
		ArrayList<Pays> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();
            // Set column
            String strCmd =
                    "select pays.id_pays, nom_pays from pays "
                    +"order by nom_pays";
            rs = stmt.executeQuery(strCmd);
            Pays paysLu = new Pays();
            while (rs.next()) {
                if (paysLu.getIdPays() != rs.getInt(1)) {
                    paysLu = new Pays(rs.getInt(1), rs.getString(2));


                    liste.add(paysLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public boolean insert(Pays object) {
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "insert into pays (nom_pays) values ('" + object.getNomPays() + "')";
            stmt.execute(strCmd);

            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean update(Pays object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "update pays set nom_pays = " + object.getNomPays() +" where id_pays = "+ object.getIdPays() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean delete(Pays object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "delete from pays where id_pays = "+ object.getIdPays() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }
}
