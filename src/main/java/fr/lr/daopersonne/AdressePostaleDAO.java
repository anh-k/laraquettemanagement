package fr.lr.daopersonne;

import fr.lr.jobspersonne.AdressePostale;
import fr.lr.service.DAO;
import fr.lr.service.DAOFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author HUYNH ANH-Khoa & Marion
 */
public class AdressePostaleDAO extends DAO<AdressePostale> {
	public AdressePostaleDAO(Connection connexion) {
		super(connexion);
	}

	@Override
	public AdressePostale getById(int id) {
		ResultSet rs;
		ArrayList<AdressePostale> liste = new ArrayList<>();

		try {
			Statement stmt = connexion.createStatement();
			

			// Set column

            String strCmd =
                    "select id_adresse_Postale, nrue, complement, nom_voie, code_postal, id_type_de_voie, "
                    + " id_ville from Adresse_Postale "
                    + " where id_Adresse_Postale = "+id+" "
                    + " order by code_postal,nom_voie";
            rs = stmt.executeQuery(strCmd);

			AdressePostale adressePostaleLu = new AdressePostale();
            while (rs.next()) {
                if (adressePostaleLu.getIdAdressePostale() != rs.getInt(1)) {



                    adressePostaleLu = new AdressePostale(rs.getInt(1), rs.getInt(2), rs.getString(4),
                            rs.getString(3), rs.getString(5),
                			DAOFactory.getVilleDAO().getById(rs.getInt(7)),
                			DAOFactory.getTypeDeVoieDAO().getById(rs.getInt(6)));


                    liste.add(adressePostaleLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste.get(0);
	}

	@Override
	public ArrayList<AdressePostale> getAll() {
		ResultSet rs;
		ArrayList<AdressePostale> liste = new ArrayList<>();

		try {
			Statement stmt = connexion.createStatement();

			// Set column

            String strCmd =
                    "select id_adresse_Postale, nrue, complement, nom_voie, code_postal, id_type_de_voie, "
                    + " id_ville from Adresse_Postale "
                    + " order by code_postal,nom_voie";
            rs = stmt.executeQuery(strCmd);

			AdressePostale adressePostaleLu = new AdressePostale();
            while (rs.next()) {
                if (adressePostaleLu.getIdAdressePostale() != rs.getInt(1)) {
                	
                		   
                	
                	adressePostaleLu = new AdressePostale(rs.getInt(1), rs.getInt(2), rs.getString(4),
                			rs.getString(3), rs.getString(5),
                			DAOFactory.getVilleDAO().getById(rs.getInt(7)),
                			DAOFactory.getTypeDeVoieDAO().getById(rs.getInt(6)));


                    liste.add(adressePostaleLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public boolean insert(AdressePostale object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "insert into adresse_postale (nrue, complement, nom_voie, "
            		+ "code_postal, id_type_de_voie, id_ville)"
            		+ " values ('" + object.getNumeroRue() + "','" + object.getComplement() + "',"
            		+ "'" + object.getNomVoie() + "','" + object.getCodePostal()+ "',"
            		+ "'" + object.getTypeDeVoie().getIdTypeDeVoie() + "','" 
            		+ object.getVille().getIdVille() + "')";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }
    

    public int insertGetId(AdressePostale object) {
        ResultSet rs;
        int inserted = 0;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "insert into adresse_postale (nrue, complement, nom_voie, "
            		+ "code_postal, id_type_de_voie, id_ville)" 
            		+ " values ('" + object.getNumeroRue() + "','" + object.getComplement() + "',"
            		+ "'" + object.getNomVoie() + "','" + object.getCodePostal()+ "',"
            		+ "'" + object.getTypeDeVoie().getIdTypeDeVoie() + "','" 
            		+ object.getVille().getIdVille() + "') RETURNING id_adresse_Postale ;";
            rs = stmt.executeQuery(strCmd);
            rs.next();
            inserted = rs.getInt(1);
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean update(AdressePostale object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column
            
            String strCmd = "update adresse_postale "
            		+ "set nrue = " + object.getNumeroRue() +" , "
                    + "set complement = " + object.getComplement() +" , "
                    + "set nom_voie = " + object.getNomVoie() +" , "
                    + "set code_postal = " + object.getCodePostal() +" , "
                    + "set id_type_de_voie = " + object.getTypeDeVoie().getIdTypeDeVoie() +" , "
                    + "set id_ville = " + object.getVille().getIdVille() +" , "
            		+ " where id_adresse_postale = "+ object.getIdAdressePostale() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean delete(AdressePostale object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "delete from AdressePostale where id_Adresse_Postale = "+ object.getIdAdressePostale() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }



}
