package fr.lr.daopersonne;

import fr.lr.jobspersonne.Ville;
import fr.lr.service.DAO;
import fr.lr.service.DAOFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author HUYNH ANH-Khoa & Marion
 */
public class VilleDAO extends DAO<Ville> {
    public VilleDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Ville getById(int id) {
        ResultSet rs;
        ArrayList<Ville> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select ville.id_ville, ville.code_insee, ville.nom_ville,ville.id_pays from ville "
                    + "join pays ON ville.id_pays = pays.id_pays "
                    + " where ville.id_ville =  "+ id +" "
                    + "order by ville.id_ville, ville.nom_ville, nom_pays";
            rs = stmt.executeQuery(strCmd);

            Ville villeLu = new Ville();
            while (rs.next()) {
                if (villeLu.getIdVille() != rs.getInt(1)) {
                    villeLu = new Ville(rs.getInt(1), rs.getString(2), rs.getString(3), DAOFactory.getPaysDAO().getById(rs.getInt(4)));
                    liste.add(villeLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste.get(0);
    }

    @Override
    public ArrayList<Ville> getAll() {
        ResultSet rs;
        ArrayList<Ville> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select ville.id_ville, ville.code_insee, ville.nom_ville,ville.id_pays from ville "
                            + "join pays ON ville.id_pays = pays.id_pays "
                    +"order by ville.nom_ville, ville.id_ville";
            rs = stmt.executeQuery(strCmd);

            Ville villeLu = new Ville();
            while (rs.next()) {
                if (villeLu.getIdVille() != rs.getInt(1)) {
                    villeLu = new Ville(rs.getInt( 1), rs.getString(2), rs.getString(3), DAOFactory.getPaysDAO().getById(rs.getInt(4)));
                    liste.add(villeLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public boolean insert(Ville object) {
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "insert into ville (code_insee, nom_ville, id_pays)"
            		+ " values ('" + object.getCodeInsee() + "',"
            		+ "'" + object.getNomVille() + "','"
            		+ object.getPays().getIdPays() + "')";
            stmt.execute(strCmd);

            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean update(Ville object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column
            
            String strCmd = "update ville "
            		+ "set code_insee = " + object.getCodeInsee() +" , "
                    + "set nom_ville = " + object.getNomVille() +" , "
                    + "set id_pays = " + object.getPays().getIdPays() +" , "
            		+ " where id_ville = "+ object.getIdVille() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean delete(Ville object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "delete from Ville where id_ville = "+ object.getIdVille() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }
    
    //---------------------------------------------------------------------------------------------

    public ArrayList<Ville> getByPays(int idPays) {
        ResultSet rs;
        ArrayList<Ville> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select ville.id_ville, ville.code_insee, ville.nom_ville,ville.id_pays from ville "
                    + " where ville.id_pays =  "+ idPays +" "
                    + "order by ville.nom_ville, ville.id_ville";
            rs = stmt.executeQuery(strCmd);

            Ville villeLu = new Ville();
            while (rs.next()) {
                if (villeLu.getIdVille() != rs.getInt(1)) {
                    villeLu = new Ville(rs.getInt(1), rs.getString(2), rs.getString(3), DAOFactory.getPaysDAO().getById(rs.getInt(4)));
                    liste.add(villeLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }
}
