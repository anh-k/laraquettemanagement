package fr.lr.daopersonne;

import fr.lr.jobspersonne.Telephone;
import fr.lr.service.DAO;
import fr.lr.service.DAOFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author HUYNH ANH-Khoa & Marion
 */
public class TelephoneDAO extends DAO<Telephone> {
    public TelephoneDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Telephone getById(int id) {
        ResultSet rs;
        ArrayList<Telephone> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select tel.id_tel, tel.numero,tel.id_personnes, tel.id_typecontact from tel "
                    +"where tel.id_tel = "+id+" order by numero";
            rs = stmt.executeQuery(strCmd);

            Telephone telephoneLu = new Telephone();
            while (rs.next()) {
                if (telephoneLu.getIdTelephone() != rs.getInt(1)) {
                	telephoneLu = new Telephone(rs.getInt(1), rs.getString(2),
                			DAOFactory.getPersonneDAO().getById(rs.getInt(3)), DAOFactory.getTypeDeContactDAO().getById(rs.getInt(4)));
                    liste.add(telephoneLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste.get(0);
    }

    @Override
    public ArrayList<Telephone> getAll() {
        ResultSet rs;
        ArrayList<Telephone> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select tel.id_tel, tel.numero,tel.id_personnes, tel.id_typecontact from tel "
                    +"order by numero";
            rs = stmt.executeQuery(strCmd);

            Telephone telephoneLu = new Telephone();
            while (rs.next()) {
                if (telephoneLu.getIdTelephone() != rs.getInt(1)) {
                	telephoneLu = new Telephone(rs.getInt(1), rs.getString(2),
                			DAOFactory.getPersonneDAO().getById(rs.getInt(3)), DAOFactory.getTypeDeContactDAO().getById(rs.getInt(4)));
                    liste.add(telephoneLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public boolean insert(Telephone object) {
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "insert into Tel (numero, id_personnes, "
            		+ "id_typecontact) values ('" + object.getLibelleTelephone() + "',"
            		+ "'" + object.getPersonne().getIdPersonne() + "',"
            		+ "'" + object.getTypeDeContact().getIdTypeDeContact() + "')";
             stmt.execute(strCmd);

            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean update(Telephone object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "update Tel "
            		+ "set numero = " + object.getLibelleTelephone() +" , "
                    + "set id_personnes = " + object.getPersonne().getIdPersonne() +" , "
            		+ " where id_typecontact = "+ object.getTypeDeContact().getIdTypeDeContact() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    @Override
    public boolean delete(Telephone object) {
        ResultSet rs;
        boolean inserted = false;

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd = "delete from tel where id_tel = "+ object.getIdTelephone() +" ";
            rs = stmt.executeQuery(strCmd);

            rs.close();
            inserted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inserted;
    }

    //---------------------------------------------------------------------------------------------
    public ArrayList<Telephone> getByPersonne(int idPersonne) {
        ResultSet rs;
        ArrayList<Telephone> liste = new ArrayList<>();

        try {
            Statement stmt = connexion.createStatement();

            // Set column

            String strCmd =
                    "select tel.id_tel, tel.numero,tel.id_personnes, tel.id_typecontact from tel "
                    + " where tel.id_personnes =  "+ idPersonne +" "
                    + "order by tel.numero";
            rs = stmt.executeQuery(strCmd);

            Telephone telephoneLu = new Telephone();
            while (rs.next()) {
                if (telephoneLu.getIdTelephone() != rs.getInt(1)) {
                	telephoneLu = new Telephone(rs.getInt(1), rs.getString(2),
                			DAOFactory.getPersonneDAO().getById(rs.getInt(3)), DAOFactory.getTypeDeContactDAO().getById(rs.getInt(4)));
                    liste.add(telephoneLu);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }
}

