package fr.lr.jobspersonne;
import fr.lr.util.CharTools;
import fr.lr.util.DateTools;

import java.time.LocalDate;
import java.util.Date;

/**
 * @author HUYNH ANH-Khoa
 */
public class Personne {

    private int idPersonne;
    private String nom;
    private String prenom;
    private LocalDate dateDeNaissance;
    private boolean femmeBoolean;
    private String profession;
    private AdressePostale adressePostale;

    public Personne(int idPersonne, String nom, String prenom, LocalDate dateDeNaissance, boolean femmeBoolean, String profession, AdressePostale adressePostale) {
        this.idPersonne = idPersonne;
        this.nom = nom.toUpperCase();
        this.prenom = CharTools.toNomPropre(prenom);
        this.dateDeNaissance = dateDeNaissance;
        this.femmeBoolean = femmeBoolean;
        this.profession = profession;
        this.adressePostale = adressePostale;
    }

    public Personne(String nom, String prenom, LocalDate dateDeNaissance, boolean femmeBoolean, String profession, AdressePostale adressePostale) {
        this.nom = nom.toUpperCase();
        this.prenom = CharTools.toNomPropre(prenom);
        this.dateDeNaissance = dateDeNaissance;
        this.femmeBoolean = femmeBoolean;
        this.profession = profession;
        this.adressePostale = adressePostale;
    }
    
    public Personne() {
    }

    public int getIdPersonne() {
        return idPersonne;
    }

    public void setIdPersonne(int idPersonne) {
        this.idPersonne = idPersonne;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public LocalDate getDateDeNaissance() {
        return dateDeNaissance;
    }

    public void setDateDeNaissance(LocalDate dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }
    public void setDateDeNaissance(Date dateDeNaissance) {
        this.dateDeNaissance = DateTools.dateToLocalDate(dateDeNaissance);
    }

    public boolean isFemmeBoolean() {
        return femmeBoolean;
    }
    public boolean getFemmeBoolean() {
        return femmeBoolean;
    }

    public void setFemmeBoolean(boolean femmeBoolean) {
        this.femmeBoolean = femmeBoolean;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public AdressePostale getAdressePostale() {
        return adressePostale;
    }

    public void setAdressePostale(AdressePostale adressePostale) {
        this.adressePostale = adressePostale;
    }


    public String getSexe () {
        String sexe = "";
        if (femmeBoolean) {
            sexe = "Femme";
        }
            else
                sexe = "Homme";
        return sexe;
    }

    public boolean setSexe (String sexe) {
        if (sexe.equals("Femme"))
            femmeBoolean = true;
        else
            femmeBoolean = false;
        return femmeBoolean;
        }


    @Override
    public String toString() {
        return  nom + " " +prenom;
    }
}
