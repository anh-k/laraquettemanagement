package fr.lr.jobspersonne;
/**
 * @author HUYNH ANH-Khoa
 */
public class Email {

    private int idEmail;
    private String libelleEmail;
    private String mdpCrypte;
    private TypeDeContact typeDeContact;
    private Personne personne;

    public Email(int idEmail, String libelleEmail, String mdpCrypte, TypeDeContact typeDeContact, Personne personne) {
        this.idEmail = idEmail;
        this.libelleEmail = libelleEmail;
        this.mdpCrypte = mdpCrypte;
        this.typeDeContact = typeDeContact;
        this.personne = personne;
    }

    public Email(String libelleEmail, Personne personne, TypeDeContact typeDeContact) {
        this.libelleEmail = libelleEmail;
        this.typeDeContact = typeDeContact;
        this.personne = personne;
    }

    public Email() {
    }

    public int getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(int idEmail) {
        this.idEmail = idEmail;
    }

    public String getLibelleEmail() {
        return libelleEmail;
    }

    public void setLibelleEmail(String libelleEmail) {
        this.libelleEmail = libelleEmail;
    }

    public String getMdpCrypte() {
        return mdpCrypte;
    }

    public void setMdpCrypte(String mdpCrypte) {
        this.mdpCrypte = mdpCrypte;
    }

    public TypeDeContact getTypeDeContact() {
        return typeDeContact;
    }

    public void setTypeDeContact(TypeDeContact typeDeContact) {
        this.typeDeContact = typeDeContact;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }
    @Override
    public String toString() {
        return  libelleEmail;
    }
}
