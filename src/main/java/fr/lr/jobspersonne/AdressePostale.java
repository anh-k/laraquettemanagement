package fr.lr.jobspersonne;
/**
 * @author HUYNH ANH-Khoa
 */
public class AdressePostale {

private int idAdressePostale;
private int numeroRue;
private String nomVoie;
private String complement;
private String codePostal;
private Ville ville;
private TypeDeVoie typeDeVoie;

    public AdressePostale(int idAdressePostale, int numeroRue, String nomVoie, String complement, String codePostal, Ville ville, TypeDeVoie typeDeVoie) {
        this.idAdressePostale = idAdressePostale;
        this.numeroRue = numeroRue;
        this.nomVoie = nomVoie;
        this.complement = complement;
        this.codePostal = codePostal;
        this.ville = ville;
        this.typeDeVoie = typeDeVoie;
    }
    
    public AdressePostale(int numeroRue, String nomVoie, String complement, String codePostal, Ville ville, TypeDeVoie typeDeVoie) {
        this.numeroRue = numeroRue;
        this.nomVoie = nomVoie;
        this.complement = complement;
        this.codePostal = codePostal;
        this.ville = ville;
        this.typeDeVoie = typeDeVoie;
    }

    public AdressePostale() {
    }

    public int getIdAdressePostale() {
        return idAdressePostale;
    }

    public void setIdAdressePostale(int idAdressePostale) {
        this.idAdressePostale = idAdressePostale;
    }

    public int getNumeroRue() {
        return numeroRue;
    }

    public void setNumeroRue(int numeroRue) {
        this.numeroRue = numeroRue;
    }

    public String getNomVoie() {
        return nomVoie;
    }

    public void setNomVoie(String nomVoie) {
        this.nomVoie = nomVoie;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public TypeDeVoie getTypeDeVoie() {
        return typeDeVoie;
    }

    public void setTypeDeVoie(TypeDeVoie typeDeVoie) {
        this.typeDeVoie = typeDeVoie;
    }

    @Override
    public String toString() {
        return numeroRue + " " + typeDeVoie.getNomTypeDeVoie() + " " + nomVoie ;
    }
}

