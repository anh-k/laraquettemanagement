package fr.lr.jobspersonne;
/**
 * @author HUYNH ANH-Khoa
 */
public class Pays {

    private int idPays;
    private String nomPays;

    public Pays(int idPays, String nomPays) {
        this.idPays = idPays;
        this.nomPays = nomPays;
    }

    public Pays(String nomPays) {
        this.nomPays = nomPays;
    }
    
    public Pays() {
    }

    public int getIdPays() {
        return idPays;
    }

    public void setIdPays(int idPays) {
        this.idPays = idPays;
    }

    public String getNomPays() {
        return nomPays;
    }

    public void setNomPays(String nomPays) {
        this.nomPays = nomPays;
    }

    @Override
    public String toString() {
        return   nomPays ;
    }
}

