package fr.lr.jobspersonne;
/**
 * @author HUYNH ANH-Khoa
 */
public class TypeDeVoie {

    private int idTypeDeVoie;
    private String nomTypeDeVoie;

    public TypeDeVoie(int idTypeDeVoie, String nomTypeDeVoie) {
        this.idTypeDeVoie = idTypeDeVoie;
        this.nomTypeDeVoie = nomTypeDeVoie;
    }

    public TypeDeVoie() {
    }

    public int getIdTypeDeVoie() {
        return idTypeDeVoie;
    }

    public void setIdTypeDeVoie(int idTypeDeVoie) {
        this.idTypeDeVoie = idTypeDeVoie;
    }

    public String getNomTypeDeVoie() {
        return nomTypeDeVoie;
    }

    public void setNomTypeDeVoie(String nomTypeDeVoie) {
        this.nomTypeDeVoie = nomTypeDeVoie;
    }

    @Override
    public String toString() {
        return  nomTypeDeVoie ;
    }
}
