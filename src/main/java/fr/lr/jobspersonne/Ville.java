package fr.lr.jobspersonne;
/**
 * @author HUYNH ANH-Khoa
 */
public class Ville {

    private int idVille;
    private String codeinsee;
    private String nomVille;
    private Pays pays;

    public Ville(int idVille, String codeinsee, String nomVille, Pays pays) {
        this.idVille = idVille;
        this.codeinsee = codeinsee;
        this.nomVille = nomVille;
        this.pays = pays;
    }

    public Ville(String codeinsee, String nomVille, Pays pays) {
        this.codeinsee = codeinsee;
        this.nomVille = nomVille;
        this.pays = pays;
    }
    
    public Ville(int idVille, String nomVille) {
        this.idVille = idVille;
        this.nomVille = nomVille;
    }

    public Ville() {
    }

    public int getIdVille() {
        return idVille;
    }

    public void setIdVille(int idVille) {
        this.idVille = idVille;
    }

    public String getNomVille() {
        return nomVille;
    }

    public void setNomVille(String nomVille) {
        this.nomVille = nomVille;
    }

    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    public String getCodeInsee() {
		return codeinsee;
	}

	public void setCodeInsee(String codeinsee) {
		this.codeinsee = codeinsee;
	}

    @Override
    public String toString() {
        return  nomVille ;
    }
}
