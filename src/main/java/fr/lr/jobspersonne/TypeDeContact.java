package fr.lr.jobspersonne;
/**
 * @author HUYNH ANH-Khoa
 */
public class TypeDeContact {
    private int idTypeDeContact;
    private String nomTypeDeContact;

    public TypeDeContact(int idTypeDeContact, String nomTypeDeContact) {
        this.idTypeDeContact = idTypeDeContact;
        this.nomTypeDeContact = nomTypeDeContact;
    }

    public TypeDeContact() {
    }

    public int getIdTypeDeContact() {
        return idTypeDeContact;
    }

    public void setIdTypeDeContact(int idTypeDeContact) {
        this.idTypeDeContact = idTypeDeContact;
    }

    public String getNomTypeDeContact() {
        return nomTypeDeContact;
    }

    public void setNomTypeDeContact(String nomTypeDeContact) {
        this.nomTypeDeContact = nomTypeDeContact;
    }

    @Override
    public String toString() {
        return  nomTypeDeContact;
    }
}
