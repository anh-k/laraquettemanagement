package fr.lr.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author HUYNH ANH-Khoa & Fran�ois
 */
public class DateTools {

	private DateTools() {
        System.out.println("hide");}
	
    private static DateTimeFormatter dateNum = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private static DateTimeFormatter dateChaine = DateTimeFormatter.ofPattern("d MMMM yyyy");

    public static LocalDate stringToDate(String date) {

        return LocalDate.parse(date, dateNum);
    }

    public static String dateToString(LocalDate date) {

        return dateNum.format(date);
    }

    public static String dateToLitteral(LocalDate date) {

        return dateChaine.format(date);
    }

    public static Date localeDateToDate(LocalDate dateToConvert) {
        //@author Mathias-Auguste "Marion" Verstaevel-Magnier
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    public static LocalDate dateToLocalDate(Date dateToConvert) {
        //@author Mathias-Auguste "Marion" Verstaevel-Magnier
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }
}